#!/bin/bash

APATH="/data/cluster/activemq/apache-activemq-5.15.12/bin"

Brokername="`$APATH/activemq list| grep '^[Aa-Zz]'| tail -1|awk -F= '{print $2}'|xargs`"

amqCounters="QueueSize"

sleeper=2

fubar=0

$APATH/activemq bstat | grep jms|grep destinationName|awk -F'=' '{print $2}'|sed 's/^ //g'|sort -u|\

while read Q

do

fubar="persistent"

amqQueue="$Q"

amqBroker="$Brokername"

getQueues(){

 result=$($APATH/activemq query -QQueue=${amqQueue} --view ${amqCounters}|grep '^[Aa-Zz]' | tail -1 | awk 'BEGIN{OFS=" ";} {print $3}')

 queueSize=$(echo $result | cut -d " " -f 1)

}


getQueues

if [ -z $queueSize ]

then

queueSize=0

fi

if echo $queueSize| grep '^[Aa-Zz]' >/dev/null 2>&1

then

queueSize=0

fi

#echo "queue size is: $queueSize"

#echo "dequeue count: $dequeueCount"

if [ $queueSize -gt 0 ]; then

                echo "$Q = $queueSize"

fi


done

#rmapp2

/bin/ssh 172.25.108.164 "cat /tmp/amqmon.tmp"